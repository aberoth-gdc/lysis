from setuptools import setup

PACKAGE_NAME: str = "lysis"


def readme():
    """
    Function to read the long description for the Lysis package.
    """
    with open("README.md") as _file:
        return _file.read()


setup(
    name=PACKAGE_NAME,
    version="1.1.2",
    description="Library for launching a Lysis bot",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aberoth-gdc/lysis",
    author="Twoshields",
    classifiers=["Topic :: Software Development :: Libraries :: Python Modules"],
    packages=[PACKAGE_NAME],
    install_requires=[
        "discord",
        "numpy",
        "tqdm",
        "pytz",
        "matplotlib",
        "python-dateutil",
        "aberothutils",
    ],
    python_requires=">=3.9",
    zip_safe=False,
)
