"""
Lysis bot is a bot for predictions
"""

import logging
import os

import discord
from discord.errors import Forbidden, HTTPException

from aberothutils import aberoth_time, real_time, real_to_string_tz, get_discord_id

from .visualization import polar
from .alignments import is_dim, has_glow, has_dim, get_glowing, get_alignments
from .predict import get_predictions
from .util import format_predictions, preprocess_input
from .bot import get_today, get_tomorrow, parse_command


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

intents: discord.Intents = discord.Intents.default()
intents.message_content = True

client: discord.Client = discord.Client(intents=intents)


@client.event
async def on_ready():
    """
    TODO
    """
    logger.info("===== LYSIS BEGIN =====")

    logger.info("LYSIS IS READY")
    logger.info("LYSIS USER ID: {}".format(client.user.id))

    servers: list[discord.Guild] = list(client.guilds)
    logger.info(f"--- Current Guilds ({len(servers)}) ---")
    
    for server in servers:
        logger.info(f"\t{server.name} ({server.id})")

        #if str(server.id) == "1057757574134501418":
            #async for member in server.fetch_members(limit=150):
            #    logger.info(f"\t\t{member.name} <{member.id}>")
            #for channel in await server.fetch_channels():
            #    logger.info(f"\t\t{channel.name} <{channel.id}>")

    logger.info("=======================")
    return


@client.event
async def on_message(message):
    """
    TODO
    """
    logger.info(
        "{}, #{}, {}: {!r}".format(
            message.guild, message.channel, message.author, message.content
        )
    )

    # Don't bother with Tavelor's messages
    if message.author == client.user:
        return

    if len(message.content) == 0:
        return

    # handle when we're not mentioned
    if get_discord_id(message.content.split()[0]) != client.user.id:
        return

    mention: str = message.author.mention

    cmd = parse_command(message)
    msg: str = await cmd(message)

    if msg != "":
        logger.debug(msg)
        # Try to do a fancy reply, otherwise just send to channel.
        try:
            await message.reply(msg)
        except Forbidden as e:
            await message.channel.send(msg)
            raise e

        except HTTPException as e:
            short_msg: str = ""
            for line in msg.split("\n"):
                if len(short_msg+line) < 1500:
                    short_msg += "\n" + line
                else:
                    try:
                        await message.reply(short_msg)
                    except Forbidden:
                        await message.channel.send(short_msg)

                    short_msg = line
            raise e

        except Exception as e:
            try:
                await message.reply(f"Ah! I'm sorry, {message.author.mention}, I see only darkness." )
            except Forbidden:
                await message.channel.send(f"Ah! I'm sorry, {message.author.mention}, I see only darkness.")
            raise e
