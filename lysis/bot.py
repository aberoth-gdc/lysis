"""
Functions relating to the bot.
"""

from datetime import date, datetime
import logging
import re
import typing

from dateutil import parser
import discord
import pytz

from aberothutils import aberoth_time, aberoth_to_string

from .predict import get_predictions, next_color_glow, next_color_dim
from .util import format_predictions, preprocess_input, format_predictions_vague

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

trivia: dict[str, str] = {
    "hello": "Wecome, {}. How can I help?",
    "hi": "Wecome, {}. How can I help?",
    "yo": "Wecome, {}. How can I help?",
    "wassup": "Wecome, {}. How can I help?",
    "wazzup": "Wecome, {}. How can I help?",
    "chicken": "I raise chickens to collect their eggs.",
    "gamble": "",
    "magic": "",
    "darklow": "Darklow is quite vile, but he is a useful tool in our fight "
    "against evil.",
    "egg": "If you see any eggs, please bring them to me.",
    "emerald": "Emeralds are beautiful, but I can't make anything useful " "with them.",
    "forstyll": "Ah, Forstyll! He was quite merry years ago. Alas, in these "
    "troubled times, he has grown serious. He feels he must protect the "
    "forest.",
    "gomald": "The squables between the villagers and the rogues do not "
    "concern me. I fear a far greater evil.",
    "hint": "I cannot offer you any more help.",
    "inala": "The squables between the villagers and the rogues do not "
    "concern me. I fear a far greater evil.",
    "orc": "The orcs are quite formitable enemies. I have created a potion of"
    " life to help you in your fight.",
    "pet": "You must earn the trust of the animal you wish to tame.",
    "pelt": "I use leather for construction of some types of jewelry.",
    "rabbit": "I use leather for construction of some types of jewelry.",
    "satyr": "A Satyr's pan pipes contain powerful magic!",
    "skaldor": "Skaldor ruled a tremendous empire. His insatiable lust for "
    "power was his undoing.",
    "tavern": "Perhaps it is only a coincidence that our troubles began the "
    "same time Tavelor arrived.",
    "tavelor": "Perhaps it is only a coincidence that our troubles began "
    "the same time Tavelor arrived.",
    "sholop": "The squables between the villagers and the rogues do not "
    "concern me. I fear a far greater evil.",
    "rogue": "The squables between the villagers and the rogues do not "
    "concern me. I fear a far greater evil.",
    "rogues": "The squables between the villagers and the rogues do not "
    "concern me. I fear a far greater evil.",
    "lich": "",
}

SUB_RE_TIMEZONE: str = "|".join(pytz.all_timezones).lower()
SUB_RE_WHEN_IS: str = "(when('s| is| are)? )"
SUB_RE_COLOR: str = "(white|black|green|red|purple|yellow|cyan|blue)"
SUB_RE_WHATS_HAPPENING: str = "(what('s| is)? )?(((glow|happen)ing|dim(med)?) )"
SUB_RE_WHAT_HAPPENED: str = "(what )?(((glow|happen)ed|dim(med)?) )"
SUB_RE_DATE: str = r"(\d\d\d\d-\d\d-\d\d|\d\d/\d\d/\d\d\d\d)"

re_color = re.compile(SUB_RE_COLOR)
re_date = re.compile(SUB_RE_DATE)

re_get_today = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHATS_HAPPENING}?(today|glow(s|ing)?|dim(s|med)?)"
            f"( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_get_yesterday = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHAT_HAPPENED}?yesterday( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_get_tomorrow = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHATS_HAPPENING}?tomorrow( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_get_this_week = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHATS_HAPPENING}?(this )?week( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_get_next_week = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHATS_HAPPENING}?next week( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_get_date = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHATS_HAPPENING}?{SUB_RE_DATE}"
            f"( (?P<timezone>{SUB_RE_TIMEZONE}))?",
        ]
    )
)

re_next_color_glow = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHEN_IS}?(the )?next ((?P<count>\d) )?"
            f"(?P<color>{SUB_RE_COLOR})( glow(s)?)?",
        ]
    )
)

re_next_color_dim = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHEN_IS}?(the )?next ((?P<count>\d) )?"
            f"(?P<color>{SUB_RE_COLOR}) dim(s)?",
        ]
    )
)

re_next_multi_glow = re.compile(
    "|".join(
        [
            f"{SUB_RE_WHEN_IS} "
            f"(?P<color_one>{SUB_RE_COLOR}) (and )?(?P<color_two>{SUB_RE_COLOR})"
            f"( aligned| glow(ing)?)?( next)",
        ]
    )
)

BETA: int = 0
HIGH: int = 1
NORMAL: int = 2
LOW: int = 3
NO_PERM: int = 1000


def _authorized(level: int, message: discord.Message) -> bool:
    logger.debug(f"Checking auth level for {message.guild.id}")
    auth_level: dict[int, int] = {
        870383494327570512: BETA,     # GDC
        825412898372321290: HIGH,     # Banabread
        888231480768200735: HIGH,     # Alliance AROC
        878722012351180801: HIGH,     # Red/Blue
        689863485554819129: NO_PERM,  # Legion PVP
        370780258141601792: NO_PERM,  # Glowbot
        1057757574134501418: NO_PERM, # Enemy guild
        1052739474339155978: HIGH,    # Grand Exchange
        1058524752353968249: HIGH,    # Lysis enemy guild
        992102384530620610: HIGH,     # Red Realm
        777635499505352754: HIGH,     # Brothers In Arms
        987130384611418172: HIGH,     # Aberoth Community
        1123270962067034284: HIGH,     # Aberoth Community 2
    }

    if message.guild is None:
        logger.error("We've received a message with no guild!")
        return False

    if message.guild.id not in auth_level:
        logger.info(f"Unknown Guild: {message.guild.name} ({message.guild.id})")
        return False

    return auth_level[message.guild.id] <= level


def parse_command(message: discord.Message) -> typing.Callable[[typing.Any], str]:
    content: list[str] = preprocess_input(message.content)
    if len(content) == 1:
        return greeting
    if len(content) == 2 and content[1].lower() in trivia:
        return get_trivia
    if re_get_today.match(" ".join(content[1:]).lower()) is not None:
        return get_today
    if re_get_yesterday.match(" ".join(content[1:]).lower()) is not None:
        return get_yesterday
    if re_get_tomorrow.match(" ".join(content[1:]).lower()) is not None:
        return get_tomorrow
    if re_get_date.match(" ".join(content[1:]).lower()) is not None:
        return get_date
    if re_next_color_dim.match(" ".join(content[1:]).lower()) is not None:
        return get_next_color_dim
    if re_next_color_glow.match(" ".join(content[1:]).lower()) is not None:
        return get_next_color

    return no_match


def not_authorized(message: discord.Message) -> str:
    """
    Give a message if the user tries to access a command from a Guild without
    sufficient permissions.
    """
    not_authorized_message: str = "Greetings, {}. I can't tell you about that."
    return not_authorized_message.format(message.author.mention)


async def no_match(message: discord.Message) -> str:
    """
    Provided when we don't know what they're talking about.
    """
    no_match_message: str = "Welcome, {}. That does not interest me."
    return no_match_message.format(message.author.mention)


async def greeting(message: discord.Message) -> str:
    """
    They only said our name.
    """
    return trivia["hi"].format(message.author.mention)


async def get_trivia(message: discord.Message) -> str:
    """
    Return some trivia that you could get from talking to the NPC in game.
    Generally only one-word phrases
    """
    content: list[str] = preprocess_input(message.content)
    return trivia[content[1].lower()].format(message.author.mention)


async def get_today(message: discord.Message) -> str:
    """
    Return a summary of orb events happening today.

    Today is defined as the unix midnight of today to the unix midnight of
    tomorrow.
    """
    if not _authorized(HIGH, message):
        return not_authorized(message)

    content = preprocess_input(message.content)
    match = re_get_today.match(" ".join(content[1:]))
    assert match is not None

    zone_str: str = match.group("timezone")
    logger.debug(f"Found zone_str: {zone_str}")
    zone = pytz.timezone("US/Eastern")
    # TODO: How to tell if no group match
    if zone_str is not None and zone_str != "":
        logger.debug("Got a new timezone!")
        zone = pytz.timezone(zone_str)

    logger.info(f"Using timezone {zone.zone}")

    today = datetime.now(zone).replace(hour=0, minute=0, second=0)

    start: int = aberoth_time(int(today.timestamp()))
    end: int = start + 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    pred_formatted: str = ""

    if not _authorized(HIGH, message):
        pred_formatted = format_predictions_vague(predictions)
    else:
        pred_formatted = format_predictions(predictions)

    msg: str = (
        f"I'm glad you asked, {message.author.mention}. "
        f"Today will have these alignments:\n{pred_formatted}"
    )

    return msg


async def get_yesterday(message: discord.Message) -> str:
    if not _authorized(BETA, message):
        return not_authorized(message)

    content = preprocess_input(message.content)
    match = re_get_yesterday.match(" ".join(content[1:]))
    assert match is not None

    zone_str: str = match.group("timezone")
    zone = pytz.timezone("US/Eastern")
    # TODO: How to tell if no group match
    if zone_str is not None and zone_str != "":
        zone = pytz.timezone(zone_str)

    today = datetime.now(zone).replace(hour=0, minute=0, second=0)

    end = aberoth_time(int(today.timestamp()))
    start = end - 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    if not _authorized(HIGH, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_tomorrow(message: discord.Message) -> str:
    """
    Return a summary of orb events happening tomorrow.

    Tomorrow is defined as the unix midnight of tomorrow to the unix midnight of
    the day after.
    """
    if not _authorized(HIGH, message):
        return not_authorized(message)

    content = preprocess_input(message.content)
    match = re_get_tomorrow.match(" ".join(content[1:]))
    assert match is not None

    zone_str: str = match.group("timezone")
    zone = pytz.timezone("US/Eastern")
    # TODO: How to tell if no group match
    if zone_str is not None and zone_str != "":
        zone = pytz.timezone(zone_str)

    today = datetime.now(zone).replace(hour=0, minute=0, second=0)

    start = aberoth_time(int(today.timestamp())) + 24 * 60 * 60 * 10
    end = start + 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    if not _authorized(HIGH, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_date(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    content = preprocess_input(message.content)

    search = re_date.search(" ".join(content[1:]))
    assert search is not None
    date_str: str = search[1]

    match = re_get_date.match(" ".join(content[1:]))
    assert match is not None
    zone_str: str = match.group("timezone")

    logger.debug(f"Found zone_str: {zone_str}")
    zone = pytz.timezone("US/Eastern")
    # TODO: How to tell if no group match
    if zone_str is not None and zone_str != "":
        logger.debug("Got a new timezone!")
        zone = pytz.timezone(zone_str)

    logger.info(f"Using timezone {zone.zone}")

    start_dt = zone.localize(parser.parse(date_str))

    logger.debug(f"Starting time is {start_dt}")

    start = aberoth_time(int(start_dt.timestamp()))
    end = start + 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    if not _authorized(HIGH, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_next_color(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp()))
    logger.debug(aberoth_to_string(start))

    match = re_next_color_glow.search(message.content)
    assert match is not None
    color = match.group("color")
    logger.debug(color)

    count_search = re_next_color_glow.search(message.content).group("count")
    if len(count_search) > 1:
        count = count_search[0]

    try:
        predictions = next_color_glow(color, start, count)
    except NotImplementedError:
        return "I'm sorry, I cannot do that yet."

    if len(predictions) == 0:
        return "I don't believe there will be another glow for {}, {}. This is most concerning.".format(
            color, message.author.mention
        )

    logger.debug(len(predictions))

    if not _authorized(HIGH, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_next_color_dim(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp()))
    logger.debug(aberoth_to_string(start))

    color = re_color.search(message.content).group("color")
    logger.debug(color)

    try:
        predictions = next_color_dim(color, start)
    except NotImplementedError:
        return "I'm sorry, I cannot do that yet!"

    if len(predictions) == 0:
        return "I don't believe there will be another dim for {}, {}. This is most concerning.".format(
            color, message.author.mention
        )

    logger.debug(len(predictions))

    if not _authorized(HIGH, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)
