"""
Abstractions for storing and sorting records of alignment events.
"""
# https://stackoverflow.com/questions/33533148/how-do-i-type-hint-a-method-with-the-type-of-the-enclosing-class
from __future__ import annotations

import itertools
import logging
import sys
import typing

from .orbs import calc_angles
from .orbs import COLORS, SHADOW, WHITE, BLACK, GREEN, RED, PURPLE, YELLOW, CYAN, BLUE

custom_thresh: dict[str, dict[str, float]] = {
    SHADOW: {
        WHITE: 1.0,
        BLACK: 1.0,
        GREEN: 1.0,
        RED: 1.0,
        PURPLE: 1.0,
        YELLOW: 1.0,
        CYAN: 1.0,
        BLUE: 1.0,
    },
}

custom_thresh[WHITE] = {
    SHADOW: custom_thresh[SHADOW][BLACK],
    BLACK: 0.20,  # Not close at all
    GREEN: 1.0,
    RED: 1.0,
    PURPLE: 1.0,
    YELLOW: 0.5,  # Perfect
    CYAN: 1.0,  # Way off, 2019-04-19
    BLUE: 0.5,  # pretty close, 2019-04-10, 2019-04-28
}

custom_thresh[BLACK] = {
    SHADOW: custom_thresh[SHADOW][BLACK],
    WHITE: custom_thresh[WHITE][BLACK],
    GREEN: 1.0,
    RED: 1.0,
    PURPLE: 1.0,
    YELLOW: 0.75,  # off, 2019-04-30
    CYAN: 1.0,
    BLUE: 1.0,
}

custom_thresh[GREEN] = {
    SHADOW: custom_thresh[SHADOW][GREEN],
    WHITE: custom_thresh[WHITE][GREEN],
    BLACK: custom_thresh[BLACK][GREEN],
    RED: 1.0,
    PURPLE: 0.75,  # Way misaligned, 2019-04-26
    YELLOW: 1.0,
    CYAN: 1.0,
    BLUE: 1.0,
}

custom_thresh[RED] = {
    SHADOW: custom_thresh[SHADOW][RED],
    WHITE: custom_thresh[WHITE][RED],
    BLACK: custom_thresh[BLACK][RED],
    GREEN: custom_thresh[GREEN][RED],
    PURPLE: 1.0,
    YELLOW: 0.65,  # close, really good one on 2019-04-26
    CYAN: 0.5,  # off, 2019-05-04
    BLUE: 1.0,
}

custom_thresh[PURPLE] = {
    SHADOW: custom_thresh[SHADOW][PURPLE],
    WHITE: custom_thresh[WHITE][PURPLE],
    BLACK: custom_thresh[BLACK][PURPLE],
    GREEN: custom_thresh[GREEN][PURPLE],
    RED: custom_thresh[RED][PURPLE],
    YELLOW: 1.0,
    CYAN: 1.0,  # 2019-03-29
    BLUE: 1.0,
}

custom_thresh[YELLOW] = {
    SHADOW: custom_thresh[SHADOW][YELLOW],
    WHITE: custom_thresh[WHITE][YELLOW],
    BLACK: custom_thresh[BLACK][YELLOW],
    GREEN: custom_thresh[GREEN][YELLOW],
    RED: custom_thresh[RED][YELLOW],
    PURPLE: custom_thresh[PURPLE][YELLOW],
    CYAN: 1.0,
    BLUE: 1.0,
}

custom_thresh[CYAN] = {
    SHADOW: custom_thresh[SHADOW][CYAN],
    WHITE: custom_thresh[WHITE][CYAN],
    BLACK: custom_thresh[BLACK][CYAN],
    GREEN: custom_thresh[GREEN][CYAN],
    RED: custom_thresh[RED][CYAN],
    PURPLE: custom_thresh[PURPLE][CYAN],
    YELLOW: custom_thresh[YELLOW][CYAN],
    BLUE: 0.0,
}

custom_thresh[BLUE] = {
    SHADOW: custom_thresh[SHADOW][BLUE],
    WHITE: custom_thresh[WHITE][BLUE],
    BLACK: custom_thresh[BLACK][BLUE],
    GREEN: custom_thresh[GREEN][BLUE],
    RED: custom_thresh[RED][BLUE],
    PURPLE: custom_thresh[PURPLE][BLUE],
    YELLOW: custom_thresh[YELLOW][BLUE],
    CYAN: custom_thresh[CYAN][BLUE],
}

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# This is 2* the space where we consider two orbs 'aligned'
THRESH: bool = 1.0

OrbSet = typing.NewType("OrbSet", set[str])
State = typing.NewType("State", dict[str, set[str]])
OrbList = typing.NewType("OrbList", list[str])


class Alignment:
    """
    Class representing a single alignment event between two orbs.
    """

    def __init__(self, orbs: OrbSet, start: int, end: int):
        assert start is not None
        assert end is not None
        self.orbs: OrbSet = orbs
        if start is None or end is None:
            raise Exception
        self.start: int = start
        self.end: int = end

    def intersects(self, other: Alignment) -> bool:
        """
        Returns whether or not this Alignment intersects another.
        """
        return self.within(other.start) or self.within(other.end)

    def within(self, timestamp: int) -> bool:
        """
        Returns whether or not the given timestamp is within this Alignment
        """
        return self.start <= timestamp <= self.end


class OrbAlignmentHistory(list[Alignment]):
    """
    A Linked List of Alignments for a single Orb

    This is the object AeonZadak came up with.
    """

    def __init__(self, color: str):
        self._color: str = color

    def get_closest(self, timestamp: int) -> Alignment:
        raise NotImplementedError

    def add(self, alignment: Alignment) -> None:
        """
        Adds a new alignment to this history. Checks to make sure that there is
        color continuity in the set.
        """
        if self._color not in alignment.orbs:
            logger.error(alignment.orbs)
            raise Exception(
                "OrbAlignmentHistories should track the history of a single orb."
            )


def is_aligned(orb_a: float, orb_b: float, thresh: float) -> bool:
    """
    Returns true if the orb angles are within a certain threshhold
    """
    # return (abs(orb_a - orb_b) + THRESH/2.0) % 180 < THRESH

    # If you get one mod == 179.9 and the other spits out 0.1, it'll tell you
    # that the abs diff
    return abs((orb_a % 180) - (orb_b % 180)) < thresh


def is_set_aligned(orbs: OrbSet, timestamp: int) -> bool:
    angles = calc_angles(timestamp)
    for orb_a in orbs:
        for orb_b in orbs:
            if orb_a == orb_b:
                continue

            if not is_aligned(
                angles[orb_a], angles[orb_b], custom_thresh[orb_a][orb_b]
            ):
                return False

    return True


_prev: int = -1
_prev_state: State = {}


def get_state(timestamp: int) -> State:
    if timestamp == this._prev:
        logger.debug("Returning previous state {}".format(_prev))
        return this._prev_state

    result: State = {}

    angles = calc_angles(timestamp)

    for i in COLORS:
        i_align: set[str] = set({})
        for j in COLORS:
            if i == j:
                continue

            if is_aligned(angles[i], angles[j], custom_thresh[i][j]):
                i_align.add(j)

        result[i] = i_align

    this._prev = timestamp
    this._prev_state = result

    return result


def get_active(timestamp: int) -> OrbList:
    """
    Returns a list of orbs that are a part of the current event.
    """
    result: OrbSet = set({})
    state: State = get_state(timestamp)

    for i in COLORS:
        result = result.union(state[i])

    return result


def has_dim(timestamp: int) -> bool:
    """
    Returns true if the orbs are dimmed at the timestamp.
    """
    return len(get_state(timestamp)[SHADOW]) > 0


def is_dim(color: str, timestamp: int) -> bool:
    """
    Returns true if the color is dimmed.
    """
    if color not in COLORS or color == SHADOW:
        raise Exception

    if has_dim(timestamp):
        return color in get_active(timestamp)

    return False


def get_dimmed(timestamp: int) -> OrbSet:
    """
    Returns a list of colors that are dimmed at the timestamp. The list can be
    empty.
    """
    if has_dim(timestamp):
        return get_active(timestamp)

    return []


def has_glow(timestamp: int) -> bool:
    """
    Returns whether or not any orb is glowing at this time. If there is a dim,
    this always returns false.
    """
    if has_dim(timestamp):
        return False

    return len(get_active(timestamp)) > 0


def is_glowing(color: str, timestamp: int) -> bool:
    """
    Returns whether an orb is glowing at the timestamp.
    """
    if color not in COLORS or color == SHADOW:
        raise Exception

    if not has_dim(timestamp):
        return color in get_active(timestamp)

    return False


def get_glowing(timestamp: int) -> OrbSet:
    """
    Returns a list of orbs that are currently glowing. If there is a dim, or no
    orb is aligned, will return an empty list.
    """
    if has_dim(timestamp):
        return []

    return get_active(timestamp)


def has_event(timestamp: int) -> bool:
    """
    Returns true if any orb is aligned with any other orb at the timestamp.
    """
    return len(get_active(timestamp)) > 0


def get_alignment_begin(orb_set: OrbSet, timestamp: int) -> int:
    """
    Searches near the timestamp for the beginning of an alignment.
    """
    attempt_range: int = 8 * 60 * 60 * 10
    resolution: int = 10
    condition: bool = False

    if is_set_aligned(orb_set, timestamp):
        start = timestamp
        end = timestamp - attempt_range
        resolution *= -1
        condition = False
    else:
        start = timestamp
        end = timestamp + attempt_range
        condition = True

    for i in range(start, end, resolution):
        if is_set_aligned(orb_set, i) == condition:
            if not condition:
                i -= resolution
            return i

    raise Exception


def get_alignment_end(orb_set: OrbSet, timestamp: int) -> int:
    """
    Searches near the timestamp for the beginning of an alignment.
    """
    # Attempt range is currently 30 minutes
    attempt_range: int = 16 * 60 * 60 * 10
    resolution: int = 10
    condition: bool = False

    # TODO: Make an overload for this
    if is_set_aligned(orb_set, timestamp):
        start = timestamp
        end = timestamp + attempt_range
        condition = False
    else:
        start = timestamp
        end = timestamp - attempt_range
        resolution *= -1
        condition = True

    for i in range(start, end, resolution):
        if is_set_aligned(orb_set, i) == condition:
            if not condition:
                i -= resolution
            return i

    logger.error(
        "Failed to find alignment end for set {} at timestamp {}.".format(
            frozenset(orb_set), timestamp
        )
    )
    logger.error("Start:%d\nEnd:%d" % (start, end))
    logger.error("Condition:%s" % str(condition))

    raise Exception


def get_aligned(timestamp: int) -> list[set[str]]:
    """
    Reduced the state to a list of sets of orbs.
    """
    result: list[set[str]] = []
    state = get_state(timestamp)
    for k in state:
        for j in state[k]:
            aligned = {k, j}
            if aligned not in result and YELLOW in aligned and SHADOW not in aligned:
                result.append(aligned)

    return result


def get_alignments(start: int, end: int) -> list[Alignment]:
    """
    Returns a list of Alignments, which are tuples containing a list of orbs
    that are aligned during a certain time.
    """

    # Adjust the search beginnig to a time where nothing is happening
    while has_event(start):
        start -= 10

    # Adjust the search end to a time where nothing is happening
    while has_event(end):
        end += 10

    result = []
    resolution = 600

    database: dict[frozenset[str, str], list[Alignment]] = {}

    aligned: set[str] = {}
    for i in range(start, end, resolution):
        align_sets = get_aligned(i)

        for orb_set in align_sets:
            if frozenset(orb_set) in database:
                exists = False
                for alignment in database[frozenset(orb_set)]:
                    if alignment.within(i):
                        exists = True

                if exists:
                    continue
            else:
                database[frozenset(orb_set)] = []

            logger.debug("Adding")

            begin = get_alignment_begin(orb_set, i)
            logger.debug("Orb set aligned %s" % str(is_set_aligned(orb_set, begin)))
            end = get_alignment_end(orb_set, i)

            database[frozenset(orb_set)].append(Alignment(orb_set, begin, end))

    return list(itertools.chain.from_iterable(database.values()))
