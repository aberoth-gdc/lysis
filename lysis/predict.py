"""
Functions for finding glows and dims
"""
import logging
import sys
import typing

from aberothutils import aberoth_to_string_tz

from .alignments import has_event, get_state, get_active, has_dim, is_glowing
from .alignments import OrbList
from .orbs import SHADOW


# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

Prediction = typing.NewType("Prediction", tuple[OrbList, int, int, bool])


def get_predictions(start: int, end: int) -> list[Prediction]:
    logger.info(
        "Getting predictions between {} and {}.".format(
            aberoth_to_string_tz(start, "US/Eastern"),
            aberoth_to_string_tz(end, "US/Eastern"),
        )
    )

    # Open the window so that we can output accurate predictions for events that
    # cross the beginning/end
    # NOTE: There is a bug where this crashes on ultra-long glows (blue/purple
    # for example
    modified_start: int = start
    modified_end: int = end

    if has_event(modified_start):
        while has_event(modified_start):
            modified_start -= 600

    if has_event(modified_end):
        while has_event(modified_end):
            modified_end += 600

    logger.info(
        "Modified prediction range between {} and {}.".format(
            aberoth_to_string_tz(modified_start, "US/Eastern"),
            aberoth_to_string_tz(modified_end, "US/Eastern"),
        )
    )

    result: list[Prediction] = []
    resolution: int = 600
    iterator = range(modified_start, modified_end, resolution)
    current: typing.Optional[Prediction] = None

    for i in iterator:
        prev_state = get_state(i - resolution)
        state = get_state(i)
        if prev_state != state:
            for j in range(i - resolution, i, 10):
                prev_state_j = get_state(j - 10)
                state_j = get_state(j)
                if state_j != prev_state_j:
                    logger.debug(i)
                    if current is not None:
                        current = Prediction(
                            (
                                current[0],
                                current[1],
                                j - 10,
                                current[3],
                            )
                        )
                        result.append(current)

                    if len(get_active(j)) == 0:
                        current = None
                    else:
                        active = get_active(j)
                        if SHADOW in active:
                            active.remove(SHADOW)
                        current = Prediction(
                            (
                                active,     # OrbList
                                j,          # Start
                                -1,         # End
                                has_dim(j), # Dim?
                            )
                        )

    logger.debug(f"Before prune: {len(result)}")
    logger.debug(f"{start}:{end}")
    pruned_results: list[Prediction] = []
    for prediction in result:
        logger.debug(f"Checking {prediction} for prune.")
        if prediction[2] < start:
            logger.debug(f"Removing out of range prediction {prediction}")
        elif prediction[1] > end:
            logger.debug(f"Removing out of range prediction {prediction}")
        else:
            pruned_results.append(prediction)

    logger.debug(f"After prune: {len(pruned_results)}")

    return pruned_results


def next_color_glow(color: str, start: int, count: int) -> Prediction:
    logger.info(
        "Getting {} {} glows from {}.".format(
            count,
            color,
            aberoth_to_string_tz(start, "US/Eastern"),
        )
    )

    max_search: int = 3 * 24 * 60 * 60  # maximum 3 days lookbehind
    i: int = 0
    if is_glowing(color, start):
        while is_glowing(color, start):
            i += 1
            start -= 10
            if i >= max_search:
                raise NotImplementedError

    max_search: int = 7 * 24 * 60 * 60  # maximum a week lookahead

    logger.info(
        "Modified prediction range between {} and {}.".format(
            aberoth_to_string_tz(start, "US/Eastern"),
            aberoth_to_string_tz(start + max_search * 10, "US/Eastern"),
        )
    )

    result = []
    event_begin = 0
    event_end = 0
    resolution = 600
    iterator = range(start, start + max_search * 10, resolution)
    stack = []
    current: typing.Optional[Prediction] = None
    for i in iterator:
        prev_state = get_state(i - resolution)
        state = get_state(i)
        if prev_state != state:
            for j in range(i - resolution, i, 10):
                prev_state_j = get_state(j - 10)
                state_j = get_state(j)
                if state_j != prev_state_j:
                    logger.debug(i)
                    if current is not None:
                        current = Prediction(
                            (
                                current[0],
                                current[1],
                                j - 10,
                                current[3],
                            )
                        )
                        result.append(current)

                    if len(get_active(j)) == 0:
                        current = None
                    else:
                        active = get_active(j)
                        if SHADOW in active:
                            active.remove(SHADOW)
                        current = Prediction(
                            (
                                active,
                                j,
                                -1,
                                has_dim(j),
                            )
                        )

    return result


def next_color_dim(color: str, start_date: int, count: int) -> Prediction:
    raise NotImplementedError
