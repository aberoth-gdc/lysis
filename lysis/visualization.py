"""
Various utility functions for creating visualizations of the Orb room
"""

import math

import matplotlib.pyplot as plt

from aberothutils import aberoth_time

from .orbs import *


def calendar(start_ts):
    pass


def polar_animated(start_ts, end_ts):
    pass


def polar(start_ts, end_ts=0):
    if end_ts != 0:
        return polar_animated(start_ts, end_ts)

    theta = [math.radians(x) for x in calc_angles(aberoth_time(start_ts))]
    # theta[1] = 2 * math.pi
    radii = [
        RADIUS_INGAME_SHADOW,
        RADIUS_INGAME_WHITE,
        RADIUS_INGAME_BLACK,
        RADIUS_INGAME_GREEN,
        RADIUS_INGAME_RED,
        RADIUS_INGAME_PURPLE,
        RADIUS_INGAME_YELLOW,
        RADIUS_INGAME_CYAN,
        RADIUS_INGAME_BLUE,
    ]
    colors = [
        "#161717",
        "#bababa",
        "#888e8f",
        "g",
        "r",
        "m",
        "y",
        "c",
        "b",
    ]
    markers = [
        "x",
        "o",
        ".",
        ".",
        ".",
        ".",
        ".",
        ".",
        ".",
    ]

    print(calc_angles(aberoth_time(start_ts)))
    print(theta)

    plt.axes(projection="polar")
    plt.scatter(theta, radii, c=colors)
    plt.show()
