"""
Functions for calculating information on the orbs based on time.
"""

import logging
import math
import os
import sys

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


PERIOD_EARTH: float = 365.256363 * 60 * 60 * 24
OFFSET_EARTH: float = 0.0

SHADOW: str = "shadow"
WHITE: str = "white"
BLACK: str = "black"
GREEN: str = "green"
RED: str = "red"
PURPLE: str = "purple"
YELLOW: str = "yellow"
CYAN: str = "cyan"
BLUE: str = "blue"

COLORS = [SHADOW, WHITE, BLACK, GREEN, RED, PURPLE, YELLOW, CYAN, BLUE]

PERIOD_SHADOW = float(os.getenv("PERIOD_SHADOW", "2360591.424"))
PERIOD_WHITE = float(os.getenv("PERIOD_WHITE", "31558149.76"))
PERIOD_BLACK = float(os.getenv("PERIOD_BLACK", "7600530.24"))
PERIOD_GREEN = float(os.getenv("PERIOD_GREEN", "19414166.4"))
PERIOD_RED = float(os.getenv("PERIOD_RED", "59354121.6"))
PERIOD_PURPLE = float(os.getenv("PERIOD_PURPLE", "374355648"))
PERIOD_YELLOW = float(os.getenv("PERIOD_YELLOW", "929292480"))
PERIOD_CYAN = float(os.getenv("PERIOD_CYAN", "2651199552"))
PERIOD_BLUE = float(os.getenv("PERIOD_BLUE", "5200761600"))

# The shadow orb is already orbiting the earth, no calculation needed
RADIUS_SHADOW = None
# The radius on the second term of the equation is implicitly 1, which is the
# radius of the earth. Thus, term 1 radius for white should be 1 as well.
RADIUS_WHITE = float(os.getenv("RADIUS_WHITE", "0"))
RADIUS_BLACK = float(os.getenv("RADIUS_BLACK", "0.3871"))
RADIUS_GREEN = float(os.getenv("RADIUS_GREEN", "0.7233"))
RADIUS_RED = float(os.getenv("RADIUS_RED", "1.542"))
RADIUS_PURPLE = float(os.getenv("RADIUS_PURPLE", "5.2044"))
RADIUS_YELLOW = float(os.getenv("RADIUS_YELLOW", "9.5826"))
RADIUS_CYAN = float(os.getenv("RADIUS_CYAN", "19.2184"))
RADIUS_BLUE = float(os.getenv("RADIUS_BLUE", "30.1104"))

RADIUS_INGAME_SHADOW = float(os.getenv("RADIUS_INGAME_SHADOW", "8"))
RADIUS_INGAME_WHITE = float(os.getenv("RADIUS_INGAME_WHITE", "16"))
RADIUS_INGAME_BLACK = float(os.getenv("RADIUS_INGAME_BLACK", "24"))
RADIUS_INGAME_GREEN = float(os.getenv("RADIUS_INGAME_GREEN", "32"))
RADIUS_INGAME_RED = float(os.getenv("RADIUS_INGAME_RED", "40"))
RADIUS_INGAME_PURPLE = float(os.getenv("RADIUS_INGAME_PURPLE", "48"))
RADIUS_INGAME_YELLOW = float(os.getenv("RADIUS_INGAME_YELLOW", "56"))
RADIUS_INGAME_CYAN = float(os.getenv("RADIUS_INGAME_CYAN", "64"))
RADIUS_INGAME_BLUE = float(os.getenv("RADIUS_INGAME_BLUE", "72"))

OFFSET_SHADOW = float(os.getenv("OFFSET_SHADOW", "0"))
# The Sun cannot have an offset. While we are in a way figuring out the initial
# positions of the orbs, these offsets are used in relationship to the sun
# itself.
OFFSET_WHITE = 0.0
# OFFSET_WHITE  = float(os.getenv("OFFSET_WHITE", str(PERIOD_WHITE*0.5)))#"15000000"))
OFFSET_BLACK = float(os.getenv("OFFSET_BLACK", "0"))
OFFSET_GREEN = float(os.getenv("OFFSET_GREEN", "0"))
OFFSET_RED = float(os.getenv("OFFSET_RED", "0"))
OFFSET_PURPLE = float(os.getenv("OFFSET_PURPLE", "0"))
OFFSET_YELLOW = float(os.getenv("OFFSET_YELLOW", "0"))
OFFSET_CYAN = float(os.getenv("OFFSET_CYAN", "0"))
OFFSET_BLUE = float(os.getenv("OFFSET_BLUE", "0"))

angle_memory: dict[int, dict[str, float]] = {}


def calc_shadow(time: int) -> float:
    """
    Calculate the current angle of the shadow orb.
    """
    return ((360.0 / (PERIOD_SHADOW)) * time + OFFSET_SHADOW) % 360.0


def calc_planet(radius: float, period: float, offset_p: float, time: int) -> float:
    """
    Calculate the angle of the an orb at a timestamp
    """
    # Calculate the x position of the planet relative to the sun
    delta_x = radius * math.cos(2.0 * math.pi * (time + offset_p) / period)
    # Modify it to be in terms of the Earth
    delta_x -= math.cos(2.0 * math.pi * (time + OFFSET_EARTH) / (PERIOD_EARTH))

    # Do the same with the Y Axis
    delta_y = radius * math.sin(2.0 * math.pi * (time + offset_p) / period)
    delta_y -= math.sin(2.0 * math.pi * (time + OFFSET_EARTH) / (PERIOD_EARTH))

    return math.degrees(math.atan2(delta_y, delta_x)) % 360.0


def calc_white(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_WHITE, PERIOD_WHITE, OFFSET_WHITE, timestamp)


def calc_black(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_BLACK, PERIOD_BLACK, OFFSET_BLACK, timestamp)


def calc_green(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_GREEN, PERIOD_GREEN, OFFSET_GREEN, timestamp)


def calc_red(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_RED, PERIOD_RED, OFFSET_RED, timestamp)


def calc_purple(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_PURPLE, PERIOD_PURPLE, OFFSET_PURPLE, timestamp)


def calc_yellow(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_YELLOW, PERIOD_YELLOW, OFFSET_YELLOW, timestamp)


def calc_cyan(timestamp: int) -> float:
    """
    Calculate the angle of the cyan orb at a timestamp
    """
    return calc_planet(RADIUS_CYAN, PERIOD_CYAN, OFFSET_CYAN, timestamp)


def calc_blue(timestamp: int) -> float:
    """
    Calculate the angle of the blue orb at a timestamp
    """
    return calc_planet(RADIUS_BLUE, PERIOD_BLUE, OFFSET_BLUE, timestamp)


def calc_color(color: str, timestamp: int) -> float:
    """
    Helper function for calling calculation functions.
    """
    if color not in COLORS:
        raise Exception

    if color == SHADOW:
        return calc_shadow(timestamp)
    if color == WHITE:
        return calc_white(timestamp)
    if color == BLACK:
        return calc_black(timestamp)
    if color == GREEN:
        return calc_green(timestamp)
    if color == RED:
        return calc_red(timestamp)
    if color == PURPLE:
        return calc_purple(timestamp)
    if color == YELLOW:
        return calc_yellow(timestamp)
    if color == CYAN:
        return calc_cyan(timestamp)
    if color == BLUE:
        return calc_blue(timestamp)

    raise Exception


_prev: int = 0
_prev_angles: dict[str, float] = {}


def calc_angles(timestamp):
    """
    Utility function returning a list of all angles
    """
    if timestamp == this._prev:
        return this._prev_angles

    result: dict[str, float] = {
        SHADOW: calc_shadow(timestamp),
        WHITE: calc_white(timestamp),
        BLACK: calc_black(timestamp),
        GREEN: calc_green(timestamp),
        RED: calc_red(timestamp),
        PURPLE: calc_purple(timestamp),
        YELLOW: calc_yellow(timestamp),
        CYAN: calc_cyan(timestamp),
        BLUE: calc_blue(timestamp),
    }

    this._prev = timestamp
    this._prev_angles = result

    return result
