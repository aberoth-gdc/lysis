"""
Utility functions for the Lysis bot
"""

import logging

import aberothutils as util

discord_id = util.discord.RE_DISCORD_ID

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def preprocess_input(content: str) -> list[str]:
    """
    Runs a client input through a series of checks and changes.
    """
    output = []

    for _, word in enumerate(content.split()):
        if isinstance(word, (float, int)):
            output.append(word)
            continue
        word = word.lower().replace("?", "")
        if util.is_discord(word):
            output.append(util.get_discord_id(word))
        elif word == "":
            continue
        else:
            output.append(word)

    return output


def format_predictions_vague(predictions) -> str:
    """
    A utility function that takes a series of predictions and formats them into
    one message, but with limited accuracy, ready to send to Discord.
    """

    msg: str = "\n"
    for prediction in predictions:
        logger.debug(" ".join(prediction[0]))
        state = "__dimmed__" if prediction[-1] else "__glowing__"

        if len(prediction[0]) == 1:
            msg += "> **" + list(prediction[0])[0].capitalize() + "**"
        elif len(prediction[0]) == 2:
            msg += "> **" + "** and **".join(list(prediction[0])).capitalize() + "**"
        else:
            msg += "> **" + "**, **".join(list(prediction[0])[:-1]).capitalize()
            msg += "**, and " + "**" + list(prediction[0])[-1] + "**"

        msg += f" will be **{state}**.\n\n"

    return msg


def format_predictions(predictions, vague=False) -> str:
    """
    A utility function that takes a series of predictions and formats them into
    one message, ready to send to Discord.
    """

    msg: str = "\n"
    for prediction in predictions:
        logger.debug(" ".join(prediction[0]))
        state = "__dimmed__" if prediction[-1] else "__glowing__"
        start = util.real_time(prediction[1])
        end = util.real_time(prediction[2])

        if len(prediction[0]) == 1:
            msg += "> **" + list(prediction[0])[0].capitalize() + "**"
        elif len(prediction[0]) == 2:
            msg += "> **" + "** and **".join(prediction[0]).capitalize() + "**"
        else:
            print(type(prediction))
            msg += "> **" + "**, **".join(list(prediction[0])[:-1]).capitalize()
            msg += " **, and " + "**" + list(prediction[0])[-1] + "**"

        if not vague:
            msg += f" will be **{state}** from <t:{start}:f> until <t:{end}:t>.\n"

    return msg
