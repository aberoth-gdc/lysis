RUN = pipenv run
PYTHON = ${RUN} python
COMPOSE = docker-compose

PACKAGE = lysis
VERSION = "1.0.0"

#clean:

integration:
	${RUN} coverage run --branch --source ${PACKAGE} -m pytest tests/integration
	${RUN} coverage report

lint:
	# https://github.com/fcfangcc/pylint-pydantic/issues/3
	${RUN} black --check .
	${RUN} pylint --extension-pkg-whitelist='pydantic' --fail-under 8.0 ${PACKAGE}

format:
	${RUN} black .

static:
	${RUN} mypy ${PACKAGE}

test: lint

unit:
	${RUN} coverage run --branch --source ${PACKAGE} -m pytest tests/unit
	${RUN} coverage report

image: test
	docker build . --tag ${PACKAGE}:${VERSION}

.PHONY: build, clean, integration, lint, sagemaker, static, test, unit
