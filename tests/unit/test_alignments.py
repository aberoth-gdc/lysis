import random
from unittest import mock

import lysis
from lysis.alignments import *


class TestGetState:
    @mock.patch("lysis.alignments.calc_angles")
    def test_some_aligned(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 0.0,
            lysis.orbs.BLACK: 50.0,
            lysis.orbs.GREEN: 50.0,
            lysis.orbs.RED: 25.0,
            lysis.orbs.PURPLE: 75.0,
            lysis.orbs.YELLOW: 100.0,
            lysis.orbs.CYAN: 125.0,
            lysis.orbs.BLUE: 150.0,
        }

        result = get_state(random.randint(0, 1000000000000))
        assert result[lysis.orbs.SHADOW] == {lysis.orbs.WHITE}
        assert result[lysis.orbs.WHITE] == {lysis.orbs.SHADOW}
        assert result[lysis.orbs.BLACK] == {lysis.orbs.GREEN}
        assert result[lysis.orbs.GREEN] == {lysis.orbs.BLACK}
        assert len(result[lysis.orbs.RED]) == 0
        assert len(result[lysis.orbs.PURPLE]) == 0
        assert len(result[lysis.orbs.YELLOW]) == 0
        assert len(result[lysis.orbs.CYAN]) == 0
        assert len(result[lysis.orbs.BLUE]) == 0

    @mock.patch("lysis.alignments.calc_angles")
    def test_all_aligned(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 0.0,
            lysis.orbs.BLACK: 0.0,
            lysis.orbs.GREEN: 0.0,
            lysis.orbs.RED: 0.0,
            lysis.orbs.PURPLE: 0.0,
            lysis.orbs.YELLOW: 0.0,
            lysis.orbs.CYAN: 0.0,
            lysis.orbs.BLUE: 0.0,
        }

        result = get_state(random.randint(0, 1000000000000))
        assert len(result[lysis.orbs.SHADOW]) == 8

    @mock.patch("lysis.alignments.calc_angles")
    def test_complex_aligned(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 25.0,
            lysis.orbs.BLACK: 50.0,
            lysis.orbs.GREEN: 50.0,
            lysis.orbs.RED: 50.5,
            lysis.orbs.PURPLE: 51.0,
            lysis.orbs.YELLOW: 100.0,
            lysis.orbs.CYAN: 125.0,
            lysis.orbs.BLUE: 150.0,
        }

        result = get_state(random.randint(0, 1000000000000))

        assert result[lysis.orbs.BLACK] == {lysis.orbs.GREEN, lysis.orbs.RED}
        assert result[lysis.orbs.GREEN] == {lysis.orbs.BLACK, lysis.orbs.RED}
        assert result[lysis.orbs.RED] == {
            lysis.orbs.BLACK,
            lysis.orbs.GREEN,
            lysis.orbs.PURPLE,
        }
        assert result[lysis.orbs.PURPLE] == {lysis.orbs.RED}

    @mock.patch("lysis.alignments.calc_angles")
    def test_none_aligned(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 20.0,
            lysis.orbs.BLACK: 40.0,
            lysis.orbs.GREEN: 60.0,
            lysis.orbs.RED: 80.0,
            lysis.orbs.PURPLE: 100.0,
            lysis.orbs.YELLOW: 120.0,
            lysis.orbs.CYAN: 140.0,
            lysis.orbs.BLUE: 160.0,
        }

        result = get_state(random.randint(0, 1000000000000))
        assert len(result) == 9
        for k in result:
            assert len(result[k]) == 0


class TestHasDim:
    def test_normal(self):
        # TODO: FIND A REAL DIM LMAO
        assert has_dim(1619833352)


class TestGetActive:
    @mock.patch("lysis.alignments.calc_angles")
    def test_all_active(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 0.0,
            lysis.orbs.BLACK: 0.0,
            lysis.orbs.GREEN: 0.0,
            lysis.orbs.RED: 0.0,
            lysis.orbs.PURPLE: 0.0,
            lysis.orbs.YELLOW: 0.0,
            lysis.orbs.CYAN: 0.0,
            lysis.orbs.BLUE: 0.0,
        }

        result = get_active(random.randint(0, 10000000000000))
        assert len(result) == 9

    @mock.patch("lysis.alignments.calc_angles")
    def test_none_active(self, mock_calc_angles):
        mock_calc_angles.return_value = {
            lysis.orbs.SHADOW: 0.0,
            lysis.orbs.WHITE: 20.0,
            lysis.orbs.BLACK: 40.0,
            lysis.orbs.GREEN: 60.0,
            lysis.orbs.RED: 80.0,
            lysis.orbs.PURPLE: 100.0,
            lysis.orbs.YELLOW: 120.0,
            lysis.orbs.CYAN: 140.0,
            lysis.orbs.BLUE: 160.0,
        }

        result = get_active(random.randint(0, 10000000000000))
        assert len(result) == 0
