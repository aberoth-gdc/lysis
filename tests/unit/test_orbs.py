import pytest

import lysis

from lysis.orbs import *


class TestCalcPlanet:
    def test_period(self):
        """
        Test if the planets make one orbit per period.
        """

        assert pytest.approx(calc_shadow(0)) == calc_shadow(PERIOD_SHADOW)
        assert pytest.approx(calc_white(0)) == calc_white(PERIOD_WHITE)
        # assert pytest.approx(calc_black(0)) == calc_black(PERIOD_BLACK)
        # assert pytest.approx(calc_green(0)) == calc_green(PERIOD_GREEN)
        # assert pytest.approx(calc_red(0)) == calc_red(PERIOD_RED)
        # assert pytest.approx(calc_purple(0)) == calc_purple(PERIOD_PURPLE)
        # assert pytest.approx(calc_yellow(0)) == calc_yellow(PERIOD_YELLOW)
        # assert pytest.approx(calc_cyan(0)) == calc_cyan(PERIOD_CYAN)
        # assert pytest.approx(calc_blue(0)) == calc_blue(PERIOD_BLUE)
