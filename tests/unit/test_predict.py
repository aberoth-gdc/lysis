import random
from unittest import mock

import lysis
from lysis.predict import *

# class TestIsDim():
#
#    def test_dim(self):
#        # https://discord.com/channels/817817785853018142/817817786364592154/836710467803807824
#        assert is_dimmed("white", 1619557706)
#        # https://discord.com/channels/817817785853018142/817817786364592154/841706284498812928
#        assert is_dimmed("white", 1620748805)
#        # https://discord.com/channels/817817785853018142/817817786364592152/843311919308734484
#        assert is_dimmed("white", 1621131625)
#
#        assert is_dimmed("green", 1619494208)
#
#        # https://discord.com/channels/817817785853018142/817817786364592154/835759676627877898
#        # TODO:
#        assert is_dimmed("red", 0)
#
#        # https://discord.com/channels/817817785853018142/817817786364592154/836710467803807824
#        assert is_dimmed("purple", 1619557706)
#        assert is_dimmed("purple", 1619714131)
#        # https://discord.com/channels/817817785853018142/817817786364592153/837858185711714306
#        assert is_dimmed("purple", 1619831352)
#        # https://discord.com/channels/817817785853018142/817817786364592153/838844421826936842
#        assert is_dimmed("purple", 1620066488)
#        # https://discord.com/channels/817817785853018142/817817786364592153/843351359290081300
#        assert is_dimmed("purple", 1621141019)
#
#        # https://discord.com/channels/817817785853018142/817817786364592153/842146044069806121
#        assert is_dimmed("yellow", 1620853648)
#
#        assert is_dimmed("cyan", 1619716760)
#        # https://discord.com/channels/817817785853018142/817817786364592153/837874018349482035
#        assert is_dimmed("cyan", 1619835120)
#        # https://discord.com/channels/817817785853018142/817817786364592153/843320555343052830
#        assert is_dimmed("cyan", 1621133677)
#
#        # https://discord.com/channels/817817785853018142/817817786364592153/842967835881635851
#        assert is_dimmed("blue", 1621049525)
