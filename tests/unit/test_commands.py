import lysis.bot as b


class TestRegex:
    def test_trivia(self):
        pass

    def test_no_match(self):
        pass

    def test_hello(self):
        pass

    def test_get_today(self):
        assert b.re_get_today.match("what's happening today") is not None
        assert b.re_get_today.match("what's happening today CST") is not None
        assert b.re_get_today.match("what is happening today") is not None
        assert b.re_get_today.match("what's glowing today") is not None
        assert b.re_get_today.match("what is glowing today") is not None
        assert b.re_get_today.match("what's dim today") is not None
        assert b.re_get_today.match("what is dim today") is not None
        assert b.re_get_today.match("what's dimmed today") is not None
        assert b.re_get_today.match("what is dimmed today") is not None
        assert b.re_get_today.match("what today") is not None
        assert b.re_get_today.match("today") is not None
        assert b.re_get_today.match("glowing") is not None
        assert b.re_get_today.match("glows") is not None
        assert b.re_get_today.match("dim") is not None
        assert b.re_get_today.match("dimmed") is not None
        assert b.re_get_today.match("dims") is not None

        match = b.re_get_today.match("what's happening today est")
        assert match is not None
        assert match.group("timezone") == "est"
        match = b.re_get_today.match("what today us/eastern")
        assert match is not None
        assert match.group("timezone") == "us/eastern"
        match = b.re_get_today.match("what's dimmed today utc")
        assert match is not None
        assert match.group("timezone") == "utc"
        match = b.re_get_today.match("today utc")
        assert match is not None
        assert match.group("timezone") == "utc"

    def test_get_tomorrow(self):
        assert b.re_get_tomorrow.match("what's happening tomorrow") is not None
        assert b.re_get_tomorrow.match("what is happening tomorrow") is not None
        assert b.re_get_tomorrow.match("what's glowing tomorrow") is not None
        assert b.re_get_tomorrow.match("what is glowing tomorrow") is not None
        assert b.re_get_tomorrow.match("what's dim tomorrow") is not None
        assert b.re_get_tomorrow.match("what is dim tomorrow") is not None
        assert b.re_get_tomorrow.match("what's dimmed tomorrow") is not None
        assert b.re_get_tomorrow.match("what is dimmed tomorrow") is not None
        assert b.re_get_tomorrow.match("what tomorrow") is not None
        assert b.re_get_tomorrow.match("what's tomorrow") is not None
        assert b.re_get_tomorrow.match("what is tomorrow") is not None
        assert b.re_get_tomorrow.match("tomorrow") is not None

        assert b.re_get_tomorrow.match("glowing") is None
        assert b.re_get_tomorrow.match("glows") is None
        assert b.re_get_tomorrow.match("dim") is None
        assert b.re_get_tomorrow.match("dimmed") is None
        assert b.re_get_tomorrow.match("dims") is None

        match = b.re_get_tomorrow.match("what's happening tomorrow est")
        assert match is not None
        assert match.group("timezone") == "est"
        match = b.re_get_tomorrow.match("what tomorrow us/eastern")
        assert match is not None
        assert match.group("timezone") == "us/eastern"
        match = b.re_get_tomorrow.match("what's dimmed tomorrow utc")
        assert match is not None
        assert match.group("timezone") == "utc"
        match = b.re_get_tomorrow.match("tomorrow utc")
        assert match is not None
        assert match.group("timezone") == "utc"

    def test_get_this_week(self):
        assert b.re_get_this_week.match("what's happening this week") is not None
        assert b.re_get_this_week.match("what is happening this week") is not None
        assert b.re_get_this_week.match("what's glowing this week") is not None
        assert b.re_get_this_week.match("what is glowing this week") is not None
        assert b.re_get_this_week.match("what's dim this week") is not None
        assert b.re_get_this_week.match("what is dim this week") is not None
        assert b.re_get_this_week.match("what's dimmed this week") is not None
        assert b.re_get_this_week.match("what is dimmed this week") is not None
        assert b.re_get_this_week.match("what this week") is not None
        assert b.re_get_this_week.match("what's this week") is not None
        assert b.re_get_this_week.match("what is this week") is not None
        assert b.re_get_this_week.match("this week") is not None
        assert b.re_get_this_week.match("week") is not None

    def test_get_next_week(self):
        assert b.re_get_next_week.match("what's happening next week") is not None
        assert b.re_get_next_week.match("what is happening next week") is not None
        assert b.re_get_next_week.match("what's glowing next week") is not None
        assert b.re_get_next_week.match("what is glowing next week") is not None
        assert b.re_get_next_week.match("what's dim next week") is not None
        assert b.re_get_next_week.match("what is dim next week") is not None
        assert b.re_get_next_week.match("what's dimmed next week") is not None
        assert b.re_get_next_week.match("what is dimmed next week") is not None
        assert b.re_get_next_week.match("what next week") is not None
        assert b.re_get_next_week.match("what's next week") is not None
        assert b.re_get_next_week.match("what is next week") is not None
        assert b.re_get_next_week.match("next week") is not None

    def test_get_date_range(self):
        pass

    def test_get_moon_phase(self):
        pass

    def test_get_time(self):
        pass

    def test_get_time_at_date(self):
        pass

    def test_get_current_time(self):
        pass

    def test_get_next_orb_event(self):
        pass

    def test_get_magic_info(self):  # ?
        """
        Mini guide for what schools have what scrolls
        """
        pass
