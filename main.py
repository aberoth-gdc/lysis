#!/usr/bin/python3

import logging
import os

import lysis


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

TOKEN: str = os.getenv("DISC_BOT_TOKEN")
logger.info(f"Discord token: {TOKEN}")

lysis.client.run(TOKEN)
